import pandas as pd
import sys

class FeatureCreator(object):
    def __init__(self, train, test):
        self.train = train
        self.test = test

    def get_mean_column(self, groupby_list, agg_by):
        mean_column = self.train.groupby(groupby_list)
        mean_column = mean_column.agg({agg_by: ['mean']})
        new_column_name = '_'.join(groupby_list) + '_' + agg_by + '_mean'
        mean_column.columns = [new_column_name]
        self.train, self.test = self.merge_dataframe(mean_column, groupby_list)
        return self.train, self.test

    def get_lag_features(self, col_list):
        df = pd.concat([self.train, self.test], sort=False, ignore_index=True)
        on_columns = ['date_block_num','shop_id','item_id']
        shifted = df[on_columns + col_list]
        shifted.columns = on_columns + [col + '_lag' for col in col_list]
        shifted['date_block_num'] += 1
        self.train, self.test = self.merge_dataframe(shifted, on_columns)
        self.train = self.train.drop_duplicates()
        self.test = self.test.drop_duplicates()
        return self.train, self.test

    def merge_dataframe(self, df_to_merge, groupby_list):
        self.train = pd.merge(self.train, df_to_merge, on=groupby_list, how='left')
        self.test = pd.merge(self.test, df_to_merge, on=groupby_list, how='left')
        return self.train, self.test

    def get_target_feature(self):
        target = 'item_cnt_month'
        cols = ['date_block_num', 'shop_id', 'item_id']
        aggregated = self.train.groupby(cols).agg({'item_cnt_day': ['sum']})
        aggregated.columns = [target]
        self.train = pd.merge(self.train, aggregated, on=cols, how='left')
        return self.train 

    def get_months(self):
        self.test['date_block_num'] = 34.0
        for df in [self.train, self.test]:
                df['months'] = df['date_block_num'] % 12
        return self.train, self.test

    def fill_nans_in_lags(self):
        lag_names = [col for col in self.test.columns if 'lag' in col]
        for lag in lag_names:
            self.train[lag].fillna(0, inplace=True)
            self.test[lag].fillna(0, inplace=True)
        return self.train, self.test


def drop_extra_features(train, test):
    test_features = list(test.columns)
    necessary_features = test_features + ['item_cnt_month']
    train = train[necessary_features]
    return train

def split_features(df, col_to_split, new_features):
    split_col = df[col_to_split].str.split(" ", expand=True)
    for i in range(len(new_features)):
        df[new_features[i]] = split_col[i]
    return df

def get_train_val_split(train):
    val = train[train['date_block_num'] == 33]
    train = train[train['date_block_num'] < 33]
    val = val[['shop_id', 'item_id', 'item_cnt_month', 'date_block_num']]
    return train, val 



if __name__ == "__main__":
    items = pd.read_csv("sales_data/items.csv")
    train = pd.read_csv("sales_data/train.csv")
    test = pd.read_csv("sales_data/test.csv").set_index("ID")
    
    fc = FeatureCreator(train, test)
    train = fc.get_target_feature() 
    train, test = fc.get_months()
    items = items.drop('item_name', axis=1)
    train, test = fc.merge_dataframe(items, None)
    
    id_list_cnt = [['shop_id'], ['item_id'], ['shop_id', 'item_id'], ['item_id', 'date_block_num']]
    for index in id_list_cnt:
        train, test = fc.get_mean_column(index, 'item_cnt_month')
    train, test = fc.get_mean_column(['item_id'], 'item_price')

    item_date_label = 'item_id_date_block_num_item_cnt_month_mean'
    #train, test = fc.get_lag_features([item_date_label])
    #train, test = fc.fill_nans_in_lags()
    test = test.drop(item_date_label, axis=1)
    print test.index 
    train = drop_extra_features(train, test) 
    
    train['item_cnt_month'] = train['item_cnt_month'].clip(0, 20)

    train.to_csv('sales_train.csv')
    test.to_csv('sales_test.csv')
