import pandas as pd
import numpy as np
import lightgbm as lgb
from scipy import stats
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LinearRegression

def get_medians_on_index(dataset, by_index):
    cols = dataset.columns
    unique_ids = dataset[by_index].unique()
    medians_df = pd.DataFrame(columns=cols, index=unique_ids)
    for by_id in unique_ids:
        for col in cols:
            relevant = dataset[(dataset[by_index] == by_id)][col]
            median = relevant.median()
            medians_df.loc[by_id][col] = median
    return medians_df

def rmse(predictions, targets):
    return np.sqrt(((predictions - targets) ** 2).mean())

def xy_split(df):
    X = df.drop(['item_cnt_month'], axis=1)
    y = df['item_cnt_month']
    return X, y

def get_train_test_preds(model, train, test):
    train = model.predict(train)
    test = model.predict(test)
    return train, test

def get_second_df(model_dict):
    # model_dict = {'name': model_preds}
    df = pd.DataFrame()
    for key in model_dict:
        df[key] = model_dict[key]
    return df

def write_submission(preds, X_test):
    prediction_df = pd.DataFrame(X_test.index, columns=['ID'])
    prediction_df['item_cnt_month'] = preds
    prediction_df.to_csv('submission.csv', index=False)
    return prediction_df

def fillnans_with_means(X_train, X_test):
    sets = [X_train, X_test]
    for dataset in sets:
        dataset.fillna(dataset.mean(), inplace=True)
    return X_train, X_test

def get_ensemble(models_list, X_train, X_test):
    train_dict = {}
    test_dict = {}
    for model in models_list:
        train, test = get_train_test_preds(model, X_train, X_test)
        str_model_name = str(model).split('(')[0]
        train_dict.update({str_model_name: train})
        test_dict.update({str_model_name: test})
    X_train_stack = get_second_df(train_dict)
    X_test_stack = get_second_df(test_dict)
    return X_train_stack, X_test_stack

if __name__ == "__main__":
    train = pd.read_csv('sales_train.csv')
    X_test = pd.read_csv('sales_test.csv')

    val = train[train['date_block_num'] == 33.]
    train = train[train['date_block_num'] < 33.]

    X_train, y_train = xy_split(train)
    X_val, y_val = xy_split(val)
    X_train, X_test = fillnans_with_means(X_train, X_test)

    gbm = lgb.LGBMRegressor(metric='RMSE')
    gbm.fit(X_train, y_train)
    
    lr = LinearRegression()
    lr.fit(X_train, y_train)

    X_train_stack, X_test_stack = get_ensemble([gbm, lr], X_train, X_val)
    
    #gbm.fit(X_train_stack, y_train)
    y_test_preds = lr.predict(X_val)
    print rmse(y_test_preds, y_val)
    #prediction_df = write_submission(y_test_preds, X_test)
