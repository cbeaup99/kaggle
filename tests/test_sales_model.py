import pandas as pd
import sales_model

def test_get_medians():
    dataset = pd.DataFrame({
        'shop_id': [0, 0, 0, 1, 1, 1],
        'item_cnt_month': [0, 1, 2, 3, 4, 5],
        'item_id': [1, 2, 3, 20, 20, 20]
        })
    medians_df = sales_model.get_medians_on_index(dataset, 'shop_id')
    zero_medians = medians_df.loc[0]
    one_medians = medians_df.loc[1]

    assert zero_medians['item_id'] == 2
    assert zero_medians['item_cnt_month'] == 1
    assert zero_medians['shop_id'] == 0

    assert one_medians['item_id'] == 20
    assert one_medians['item_cnt_month'] == 4
    assert one_medians['shop_id'] == 1
