import pandas as pd
import features

def test_train_val_split():
    initial_train = pd.DataFrame({
        'shop_id': [1, 2, 3, 4],
        'item_id': [0, 5, 5, 0],
        'item_cnt_month': [0, 1, 0, 1],
        'date_block_num': [31, 31, 33, 33]
        })
    train, val = features.get_train_val_split(initial_train)
    assert len(train) == 2
    assert len(val) == 2
    assert list(train['shop_id']) == [1, 2]
    assert list(val['shop_id']) == [3, 4]
    for col in list(val.columns):
        assert col in list(initial_train.keys())

def test_mean_encoder():
    train = pd.DataFrame({
        'item_cnt_month': [0, 1, 2, 3, 4],
        'shop_id': [0, 0, 0, 1, 1]
        })
    val = pd.DataFrame({
        'shop_id': [1, 0, 1, 0, 0]
        })
    fc = features.FeatureCreator(train, val)
    new_train, new_val = fc.mean_encoder(['shop_id'], 'item_cnt_month')
    sets = [new_train, new_val]
    for dataset in sets:
        col_name = 'shop_id_item_cnt_month_mean'
        zero_id_means = dataset.loc[(dataset['shop_id'] == 0)][col_name]
        one_id_means = dataset.loc[(dataset['shop_id'] == 1)][col_name]
        assert list(zero_id_means) == [1., 1., 1.] 
        assert list(one_id_means) == [3.5, 3.5]
    assert len(new_val) == len(val)

def test_get_lags():
    train = pd.DataFrame({
        'date_block_num': [32, 33, 33],
        'shop_id': [1, 1, 1],
        'item_id': [5, 5, 5],
        'item_cnt_month': [10, 20, 30],
        'item_price': [5, 10, 15]
        })
    test = pd.DataFrame({
        'date_block_num': [34],
        'shop_id': [1],
        'item_id': [5],
        'item_price': [5],
        'item_cnt_month': [10]
        })
    fc = features.FeatureCreator(train, test)
    features = ['item_cnt_month', 'item_price']
    lags_train, lags_test = fc.get_lag_features(features)
    for label in features:
        assert lags_train[label + '_lag'][1] == lags_train[label][0]
        assert lags_test[label + '_lag'][0] == lags_train[label][1]
        for df_with_lags in [lags_train, lags_test]:
            assert label + '_lag' in df_with_lags.columns
